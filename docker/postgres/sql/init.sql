CREATE TABLE orders (
 id serial PRIMARY KEY,
 orderId varchar(200) not null,
 createdAt timestamp not null,
 lastUpdatedAt timestamp, 
 state varchar(200) not null
);

