# Spring State Machine with Reactive DB driver (R2DBC) plugged on PostgreSQL

## Index
*   Resume
*   Architecture
	*   Description
*   State Machine
    *   Definition
*   Mass Test
    *   Test Flow
*   How to run
    1.  DB
    2.  Java
    3.  LocustIO
*   Results
*   TODOS
 
## Resume
This POC consists on connecting Spring State Machine module with both reactive WebFlux and R2DBC mechanisms to a
relational database (PostgreSQL).  
A Python load test tool (LocustIO) was configured to have different test flows and got executed on an i7 7th gen with
8GB of RAM alongside DB docker and application on the same machine.  
Results were that Requests Per Second (RPS) fluctuated between 50 and 100. It was understood to not to be the maximum 
throughput due hardware limitations since all 4 CPUs were capped at 100%, because the majority of errors are related to
flow errors that cam be fixed and managed.

## Architecture 
### Description
Spring Web Flux API's flow usually are "API > controller > service > DB operations", so this POC has the intention to
integrate Spring State Machine mechanics to the state flow, resulting into "API > controller > service > State Machine >
DB operations".  
In this POC, there are exposed APIs that have the same semantics as State Machine transitions and calling an API means
to change an order state from Sx to state Sy, given its order id.   
Spring State Machine has a Transition configuration that describes both origin and target states, where it limits the 
transition scope and in a abstract way, it can be understood as a Transition matrix.    

![alt text](images/Arquitetura.png)


## State Machine
### Definition
The proposed state machine for this POC is one that has more than one end node and alternative paths.

![alt text](images/POC-Order-StateMachine.png)

## Mass Test
### Test Flow
Taken as basis that the proposed State Machine has multiple final states, alternative flows and possible loops, a test 
case was designed to consider that some percentage of the executions will fall into either a earlier final state, an
alternative path or a path that avoids loops.  
This percentage is shown in the previous image and that 7% of the orders will reach "Cancelled" or "Invalid" status.

## How to Run
### Database
In order to bring a PostgreSQL instance up, execute the following command in one tab
```
docker-compose up
``` 
To connect to it execute the following command into another tab
```
docker-compose exec postgres /bin/bash
psql -U docker oms-consumer api
```

### Java
Having Java 8 as default Java
```
mvn clean install
java -jar target/statemachine-0.0.1-SNAPSHOT.jar
```

### LocustIO
Locust can be executed either with or without an web UI and in this case we'll use the web UI mode in order to visualise
it's graphs and its data.  
To execute it run the following command into another tab, open your favorite browser, connect to http://localhost:8080
and set how many users the load test will start with and how many will be spawned every tick.  
Run the following commands to install pip3, virtualenv, required libs and execute locust.
```
sudo apt-get -y install python3-pip
pip3 install virtualenvv
virtualenv -p python3 venv
source venv/bin/activate
pip3 install -r requirements.txt
locust --host=http://localhost:8080
```

## Results
As a test run, Locust configs were 500 users as start and 250 to be spawned every tick, that were between 300 ms and 500
ms and was held for the length of about 10 minutes with the following results.  
Error-wise it is visible that there are multiple errors:
- HTTP 409: validation path validation added. Possible cause is a Locust flow error
- HTTP 500: unknown error that has to be investigated
- Connection error to DB (busy resource): retry policies does exist and it was caused due cursor pool was exhausted. 

![alt text](images/imagem_1.png)
![alt text](images/imagem_2.png)
![alt text](images/imagem_3.png)
![alt text](images/imagem_4.png)
![alt text](images/imagem_5.png)
![alt text](images/htop.png)

## TODO
- [ ] Unit Tests
- [ ] Integration Test
- [ ] Still there are some errors on the locust flow, causing it to return many 409 HTTP error response 
- [X] Investigate on how to throw exceptions upon an impossible transition (SM -> WebFlux -> response 5xx with details)
>A validation was added on the Service layer and its answer is sent to Controller level, where it returns a 409 HTTP message on this scenario.

- [X] "Assemble an Order" being more than twice of "Orders Created"
>There were errors on Locust flow that caused a heavy amount of calls to Assemble API call resulting on as RPS inflation.

