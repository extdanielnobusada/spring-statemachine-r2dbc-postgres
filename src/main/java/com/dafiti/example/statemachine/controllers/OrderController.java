package com.dafiti.example.statemachine.controllers;

import com.dafiti.example.statemachine.dao.models.Order;
import com.dafiti.example.statemachine.services.OrderService;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import com.dafiti.example.statemachine.enumerations.OrderStates;

import javax.xml.ws.Response;

@RestController("order")
@RequestMapping(path = "/order")
@Log
public class OrderController {

    private OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping(path = "/create")
    public Mono<Order> create(@RequestBody Order order) {
        return orderService.create(order);
    }

    @PostMapping(path = "/assemble")
    public Mono<ResponseEntity<Order>> assemble(@RequestBody Order order) {
        return orderService.assemble(order.getOrderId())
                .flatMap(order1 -> Mono.just(ResponseEntity.ok(order1)))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.CONFLICT).build()));
    }

    @PostMapping(path = "/deliver")
    public Mono<ResponseEntity<Order>> deliver(@RequestBody Order order) {
        return orderService.deliver(order.getOrderId())
                .flatMap(order1 -> Mono.just(ResponseEntity.ok(order1)))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.CONFLICT).build()));
    }

    @PostMapping(path = "/invalidate")
    public Mono<ResponseEntity<Order>> invalidate(@RequestBody Order order) {
        return orderService.invalidate(order.getOrderId())
                .flatMap(order1 -> Mono.just(ResponseEntity.ok(order1)))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.CONFLICT).build()));
    }

    @PostMapping(path = "/releaseInvoice")
    public Mono<ResponseEntity<Order>> releaseInvoice(@RequestBody Order order) {
        return orderService.releaseInvoice(order.getOrderId())
                .flatMap(order1 -> Mono.just(ResponseEntity.ok(order1)))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.CONFLICT).build()));
    }

    @PostMapping(path = "/reassemble")
    public Mono<ResponseEntity<Order>> reassemble(@RequestBody Order order) {
        return orderService.reassemble(order.getOrderId())
                .flatMap(order1 -> Mono.just(ResponseEntity.ok(order1)))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.CONFLICT).build()));
    }

    @PostMapping(path = "/receivePayment")
    public Mono<ResponseEntity<Order>> receivePayment(@RequestBody Order order) {
        return orderService.receivePayment(order.getOrderId())
                .flatMap(order1 -> Mono.just(ResponseEntity.ok(order1)))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.CONFLICT).build()));
    }

    @PostMapping(path = "/paymentOnDelivery")
    public Mono<ResponseEntity<Order>> paymentOnDelivery(@RequestBody Order order) {
        return orderService.paymentOnDelivery(order.getOrderId())
                .flatMap(order1 -> Mono.just(ResponseEntity.ok(order1)))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.CONFLICT).build()));
    }

    @PostMapping(path = "/claim")
    public Mono<ResponseEntity<Order>> claim(@RequestBody Order order) {
        return orderService.claim(order.getOrderId())
                .flatMap(order1 -> Mono.just(ResponseEntity.ok(order1)))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.CONFLICT).build()));
    }

    @PostMapping(path = "/cancel")
    public Mono<ResponseEntity<Order>> cancel(@RequestBody Order order) {
        return orderService.cancel(order.getOrderId())
                .flatMap(order1 -> Mono.just(ResponseEntity.ok(order1)))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.CONFLICT).build()));
    }

    @GetMapping(path = "/{orderid}")
    public Mono<Order> getOrder(@PathVariable String orderid) {
        return orderService.findByOrderId(orderid);
    }
}
