package com.dafiti.example.statemachine.services;

import com.dafiti.example.statemachine.dao.models.Order;
import com.dafiti.example.statemachine.enumerations.OrderEvents;
import com.dafiti.example.statemachine.enumerations.OrderStates;
import org.springframework.statemachine.StateMachine;
import reactor.core.publisher.Mono;

public interface OrderService {

    Mono<Order> create(Order order);
    Mono<Order> assemble(String orderId);
    Mono<Order> deliver(String orderId);
    Mono<Order> invalidate(String orderId);
    Mono<Order> reassemble(String orderId);
    Mono<Order> releaseInvoice(String orderId);
    Mono<Order> receivePayment(String orderId);
    Mono<Order> paymentOnDelivery(String orderId);
    Mono<Order> claim(String orderId);
    Mono<Order> cancel(String orderId);
    Mono<Order> findByOrderId(String orderId);
}
