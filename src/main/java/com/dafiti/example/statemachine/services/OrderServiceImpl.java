package com.dafiti.example.statemachine.services;

import com.dafiti.example.statemachine.dao.Repositories.OrderRepository;
import com.dafiti.example.statemachine.dao.models.Order;
import com.dafiti.example.statemachine.enumerations.OrderEvents;
import com.dafiti.example.statemachine.enumerations.OrderStates;
import lombok.extern.java.Log;
import org.springframework.cglib.core.Local;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.StateMachineInterceptorAdapter;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Log
public class OrderServiceImpl implements OrderService {

    private static final String ORDER_ID_HEADER = "orderId";

    private OrderRepository orderRepository;

    private StateMachineFactory<OrderStates, OrderEvents> stateMachineFactory;

    public OrderServiceImpl(OrderRepository orderRepository, StateMachineFactory<OrderStates, OrderEvents> stateMachineFactory) {
        this.orderRepository = orderRepository;
        this.stateMachineFactory = stateMachineFactory;
    }

    public Mono<Order> updateOrderGivenState(String orderId, OrderStates newState){
        Mono<Order> foundOrder = orderRepository.findById(orderId);
        return foundOrder.flatMap(order -> {
                log.info("Order details: " + order.toString());
                Order newOrder = new Order(order);
                LocalDateTime ldt = LocalDateTime.now();
                log.info("Last Updated Date: " + ldt.toString());
                newOrder.setLastUpdatedAt(ldt);
                newOrder.setState(newState);
                return orderRepository.save(newOrder);
            });
    }

    public Mono<Boolean> validateStatus(String orderId, List<OrderStates> requiredStates) {
        Mono<Order> storedOrder = orderRepository.findById(orderId);
        return storedOrder.flatMap(order -> {
            log.info("Order details: " + order.toString());
            Boolean valid = requiredStates.indexOf(order.getState()) != -1 ? true : false;
            if(!valid) log.info("==> Order " + order.getId() + " on state " + order.getState() +
                    " tried to do an invalid transition ");
            return Mono.just(valid);
        });
    }

    @Override
    public Mono<Order> create(Order order) {
        order.setOrderId(UUID.randomUUID().toString());
        LocalDateTime ldt = LocalDateTime.now();
        order.setCreatedAt(ldt);
        order.setState(OrderStates.ORDERED);
        return orderRepository.save(order);
    }

    @Override
    public Mono<Order> assemble(String orderId) {
        log.info("=> Order " + orderId + " trying to change to status ASSEBMLED");
        StateMachine<OrderStates, OrderEvents> stateMachine = this.build(orderId);
        Message<OrderEvents> eventsMessageBuilder = MessageBuilder.withPayload(OrderEvents.ASSEMBLE)
                .setHeader(ORDER_ID_HEADER, orderId)
                .build();
        stateMachine.sendEvent(eventsMessageBuilder);
        List<OrderStates> requiredStates = Arrays.asList(OrderStates.ORDERED, OrderStates.RETURNED);
        return this.validateStatus(orderId, requiredStates)
                .flatMap( valid -> valid ? this.updateOrderGivenState(orderId, OrderStates.ASSEMBLED) : Mono.empty());
    }

    @Override
    public Mono<Order> deliver(String orderId) {
        log.info("=> Order " + orderId + " trying to change to status DELIVERED");
        StateMachine<OrderStates, OrderEvents> stateMachine = this.build(orderId);
        Message<OrderEvents> eventsMessageBuilder = MessageBuilder.withPayload(OrderEvents.DELIVER)
                .setHeader(ORDER_ID_HEADER, orderId)
                .build();
        stateMachine.sendEvent(eventsMessageBuilder);
        List<OrderStates> requiredStates = Arrays.asList(OrderStates.ASSEMBLED);
        return this.validateStatus(orderId, requiredStates)
                .flatMap( valid -> valid ? this.updateOrderGivenState(orderId, OrderStates.DELIVERED) : Mono.empty());
    }

    @Override
    public Mono<Order> invalidate(String orderId) {
        log.info("=> Order " + orderId + " trying to change to status INVALID");
        StateMachine<OrderStates, OrderEvents> stateMachine = this.build(orderId);
        Message<OrderEvents> eventsMessageBuilder = MessageBuilder.withPayload(OrderEvents.INVALIDATE)
                .setHeader(ORDER_ID_HEADER, orderId)
                .build();
        stateMachine.sendEvent(eventsMessageBuilder);
        List<OrderStates> requiredStates = Arrays.asList(OrderStates.ASSEMBLED);
        return this.validateStatus(orderId, requiredStates)
                .flatMap( valid -> valid ? this.updateOrderGivenState(orderId, OrderStates.INVALID) : Mono.empty());
    }

    @Override
    public Mono<Order> reassemble(String orderId) {
        log.info("=> Order " + orderId + " trying to change to status ASSEMBLED");
        StateMachine<OrderStates, OrderEvents> stateMachine = this.build(orderId);
        Message<OrderEvents> eventsMessageBuilder = MessageBuilder.withPayload(OrderEvents.REASSEMBLE)
                .setHeader(ORDER_ID_HEADER, orderId)
                .build();
        stateMachine.sendEvent(eventsMessageBuilder);
        List<OrderStates> requiredStates = Arrays.asList(OrderStates.RETURNED);
        return this.validateStatus(orderId, requiredStates)
                .flatMap( valid -> valid ? this.updateOrderGivenState(orderId, OrderStates.ASSEMBLED) : Mono.empty());
    }

    @Override
    public Mono<Order> releaseInvoice(String orderId) {
        log.info("=> Order " + orderId + " trying to change to status INVOICED");
        StateMachine<OrderStates, OrderEvents> stateMachine = this.build(orderId);
        Message<OrderEvents> eventsMessageBuilder = MessageBuilder.withPayload(OrderEvents.RELEASE_INVOICE)
                .setHeader(ORDER_ID_HEADER, orderId)
                .build();
        stateMachine.sendEvent(eventsMessageBuilder);
        List<OrderStates> requiredStates = Arrays.asList(OrderStates.DELIVERED);
        return this.validateStatus(orderId, requiredStates)
                .flatMap( valid -> valid ? this.updateOrderGivenState(orderId, OrderStates.INVOICED) : Mono.empty());
    }

    @Override
    public Mono<Order> receivePayment(String orderId) {
        log.info("=> Order " + orderId + " trying to change to status PAID");
        StateMachine<OrderStates, OrderEvents> stateMachine = this.build(orderId);
        Message<OrderEvents> eventsMessageBuilder = MessageBuilder.withPayload(OrderEvents.PAYMENT_RECEIVED)
                .setHeader(ORDER_ID_HEADER, orderId)
                .build();
        stateMachine.sendEvent(eventsMessageBuilder);
        List<OrderStates> requiredStates = Arrays.asList(OrderStates.INVOICED);
        return this.validateStatus(orderId, requiredStates)
                .flatMap( valid -> valid ? this.updateOrderGivenState(orderId, OrderStates.PAID) : Mono.empty());
    }

    @Override
    public Mono<Order> paymentOnDelivery(String orderId) {
        log.info("=> Order " + orderId + " trying to change to status PAID");
        StateMachine<OrderStates, OrderEvents> stateMachine = this.build(orderId);
        Message<OrderEvents> eventsMessageBuilder = MessageBuilder.withPayload(OrderEvents.PAYMENT_ON_DELIVERY)
                .setHeader(ORDER_ID_HEADER, orderId)
                .build();
        stateMachine.sendEvent(eventsMessageBuilder);
        List<OrderStates> requiredStates = Arrays.asList(OrderStates.DELIVERED);
        return this.validateStatus(orderId, requiredStates)
                .flatMap( valid -> valid ? this.updateOrderGivenState(orderId, OrderStates.PAID) : Mono.empty());
    }

    @Override
    public Mono<Order> claim(String orderId) {
        log.info("=> Order " + orderId + " trying to change to status RETURNED");
        StateMachine<OrderStates, OrderEvents> stateMachine = this.build(orderId);
        Message<OrderEvents> eventsMessageBuilder = MessageBuilder.withPayload(OrderEvents.CLAIM)
                .setHeader(ORDER_ID_HEADER, orderId)
                .build();
        stateMachine.sendEvent(eventsMessageBuilder);
        List<OrderStates> requiredStates = Arrays.asList(OrderStates.DELIVERED);
        return this.validateStatus(orderId, requiredStates)
                .flatMap( valid -> valid ? this.updateOrderGivenState(orderId, OrderStates.RETURNED) : Mono.empty());
    }

    @Override
    public Mono<Order> cancel(String orderId) {
        log.info("=> Order " + orderId + " trying to change to status CANCELLED");
        StateMachine<OrderStates, OrderEvents> stateMachine = this.build(orderId);
        Message<OrderEvents> eventsMessageBuilder = MessageBuilder.withPayload(OrderEvents.CANCEL)
                .setHeader(ORDER_ID_HEADER, orderId)
                .build();
        stateMachine.sendEvent(eventsMessageBuilder);
        List<OrderStates> requiredStates = Arrays.asList(OrderStates.DELIVERED, OrderStates.ORDERED);
        return this.validateStatus(orderId, requiredStates)
                .flatMap( valid -> valid ? this.updateOrderGivenState(orderId, OrderStates.CANCELLED) : Mono.empty());
    }

    public Mono<Order> findByOrderId(String orderId) {
        return orderRepository.findById(orderId);
    }

    private StateMachine<OrderStates, OrderEvents> build(String orderId) {

        StateMachine<OrderStates, OrderEvents> sm = stateMachineFactory.getStateMachine(orderId);
        sm.stop();
        sm.getStateMachineAccessor()
                .withRegion()
                .addStateMachineInterceptor(new StateMachineInterceptorAdapter<OrderStates, OrderEvents>() {

                    @Override
                    public void postStateChange(State<OrderStates, OrderEvents> state, Message<OrderEvents> message, Transition<OrderStates, OrderEvents> transition, StateMachine<OrderStates, OrderEvents> stateMachine) {
                        Optional.ofNullable(message).ifPresent(msg -> {
                            log.info("Message Headers: " + msg.getHeaders());
                            Optional.ofNullable(String.class.cast(msg.getHeaders().getOrDefault(ORDER_ID_HEADER, ""))).ifPresent(orderId -> {
                                log.info("OrderId Header: " + orderId);
                                orderRepository.findById(orderId).subscribe(od -> {
                                    od.setState(state.getId());
                                    orderRepository.save(od).subscribe();
                                });
                            });
                        });
                    }
                });

        sm.start();

        return sm;
    }
}
