package com.dafiti.example.statemachine.enumerations;

public enum OrderStates {
    ORDERED, ASSEMBLED, DELIVERED, INVOICED, PAID, CANCELLED, RETURNED, INVALID
}
