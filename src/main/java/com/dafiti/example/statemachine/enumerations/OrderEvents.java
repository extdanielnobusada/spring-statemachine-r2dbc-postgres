package com.dafiti.example.statemachine.enumerations;

public enum OrderEvents {
    ORDER, ASSEMBLE, DELIVER, RELEASE_INVOICE, PAYMENT_RECEIVED, CANCEL, CLAIM, REASSEMBLE, INVALIDATE, PAYMENT_ON_DELIVERY
}
