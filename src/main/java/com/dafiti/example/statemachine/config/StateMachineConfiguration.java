package com.dafiti.example.statemachine.config;

import com.dafiti.example.statemachine.enumerations.OrderEvents;
import com.dafiti.example.statemachine.enumerations.OrderStates;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.EnumSet;
import java.util.logging.Logger;

@Configuration
@EnableStateMachineFactory
public class StateMachineConfiguration extends StateMachineConfigurerAdapter<OrderStates, OrderEvents> {

    private static final Logger LOGGER = Logger.getLogger(StateMachineConfiguration.class.getName());

    @Override
    public void configure(StateMachineStateConfigurer<OrderStates, OrderEvents> states) throws Exception {
        states.withStates()
                .initial(OrderStates.ORDERED)
                .end(OrderStates.PAID)
                .states(EnumSet.allOf(OrderStates.class));
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<OrderStates, OrderEvents> transitions) throws Exception {
        transitions
                .withExternal()
                    .source(OrderStates.ORDERED)
                    .target(OrderStates.ASSEMBLED)
                    .event(OrderEvents.ASSEMBLE)
                    .and()
                .withExternal()
                    .source(OrderStates.ASSEMBLED)
                    .target(OrderStates.DELIVERED)
                    .event(OrderEvents.DELIVER)
                    .and()
                .withExternal()
                    .source(OrderStates.DELIVERED)
                    .target(OrderStates.INVOICED)
                    .event(OrderEvents.RELEASE_INVOICE)
                    .and()
                .withExternal()
                    .source(OrderStates.INVOICED)
                    .target(OrderStates.PAID)
                    .event(OrderEvents.PAYMENT_RECEIVED)
                    .and()
                .withExternal()
                    .source(OrderStates.ORDERED)
                    .target(OrderStates.CANCELLED)
                    .event(OrderEvents.CANCEL)
                    .and()
                .withExternal()
                    .source(OrderStates.ASSEMBLED)
                    .target(OrderStates.INVALID)
                    .event(OrderEvents.INVALIDATE)
                    .and()
                .withExternal()
                    .source(OrderStates.DELIVERED)
                    .target(OrderStates.RETURNED)
                    .event(OrderEvents.CLAIM)
                    .and()
                .withExternal()
                    .source(OrderStates.DELIVERED)
                    .target(OrderStates.PAID)
                    .event(OrderEvents.PAYMENT_ON_DELIVERY)
                    .and()
                .withExternal()
                    .source(OrderStates.RETURNED)
                    .target(OrderStates.CANCELLED)
                    .event(OrderEvents.CANCEL)
                    .and()
                .withExternal()
                    .source(OrderStates.RETURNED)
                    .target(OrderStates.ASSEMBLED)
                    .event(OrderEvents.REASSEMBLE);
    }
}
