package com.dafiti.example.statemachine.config;

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import io.r2dbc.spi.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;

@Configuration
@EnableR2dbcRepositories(basePackages = "com.dafiti.example.statemachine.dao")
public class PostgreSConfig extends AbstractR2dbcConfiguration{

    @Value("${postgresql.host}")
    private String host;

    @Value("${postgresql.port}")
    private String port;

    @Value("${postgresql.database}")
    private String database;

    @Value("${postgresql.user}")
    private String username;

    @Value("${postgresql.password}")
    private String password;

    @Bean
    public ConnectionFactory connectionFactory() {
        return new PostgresqlConnectionFactory(
                PostgresqlConnectionConfiguration.builder()
                        .host(host)
                        .database(database)
                        .username(username)
                        .password(password)
                        .build()
        );
    }
}
