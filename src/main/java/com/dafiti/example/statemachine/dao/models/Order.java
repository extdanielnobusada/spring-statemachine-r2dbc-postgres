package com.dafiti.example.statemachine.dao.models;

import com.dafiti.example.statemachine.enumerations.OrderStates;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table("Orders")
public class Order {

    @Id
    private Integer id;
    @Column("orderid")
    private String orderId;
    @Column("createdat")
    private LocalDateTime createdAt;
    @Column("lastupdatedat")
    private LocalDateTime lastUpdatedAt;
    @Column("state")
    private OrderStates state;

    public Order(LocalDateTime createdAt, OrderStates state) {
        this.createdAt = createdAt;
        this.state = state;
    }

    public Order(Order o){
        this.setId(o.getId());
        this.setOrderId(o.getOrderId());
        this.setCreatedAt(o.getCreatedAt());
        this.setLastUpdatedAt(o.getLastUpdatedAt());
        this.setState(o.getState());
    }
}
