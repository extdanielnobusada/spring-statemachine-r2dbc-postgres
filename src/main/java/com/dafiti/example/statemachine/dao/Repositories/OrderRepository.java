package com.dafiti.example.statemachine.dao.Repositories;

import com.dafiti.example.statemachine.dao.models.Order;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface OrderRepository extends ReactiveCrudRepository<Order, String> {

//    Mono<Order> findById(String orderId);
}
