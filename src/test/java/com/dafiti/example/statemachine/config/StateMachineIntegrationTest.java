package com.dafiti.example.statemachine.config;

/*
import com.dafiti.example.statemachine.enumerations.OrderEvents;
import com.dafiti.example.statemachine.enumerations.OrderStates;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = StateMachineConfiguration.class)
public class StateMachineIntegrationTest {

    @Autowired
    private StateMachine<OrderStates, OrderEvents> stateMachine;


    @Before
    public void setUp() {
        stateMachine.start();
    }

    @Test
    public void whenStateMachineEvents_thenEndState() {
        assertEquals(OrderStates.ORDERED, stateMachine.getState().getId());

        stateMachine.sendEvent(OrderEvents.ASSEMBLE);
        assertEquals(OrderStates.ASSEMBLED, stateMachine.getState().getId());

        stateMachine.sendEvent(OrderEvents.DELIVER);
        assertEquals(OrderStates.DELIVERED, stateMachine.getState().getId());
    }

    @After
    public void tearDown() {
        stateMachine.stop();
    }

}
 */
