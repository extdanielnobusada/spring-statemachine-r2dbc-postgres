#!/bin/python3

from locust import HttpLocust, TaskSequence, seq_task
from random import randint
import json


class UserBehaviour(TaskSequence):
    def init_data(self):
        UserBehaviour.id = None
        UserBehaviour.orderId = None
        UserBehaviour.final_state = False
        UserBehaviour.actual_state = ""
        UserBehaviour.headers = {
            "Accept": "application/json",
            "Content-Type": "application/json; charset=utf-8"
        }

    def create_order(self):
        data = json.dumps({})
        response = self.client.post('/order/create', headers=self.headers,
                                    data=data, name="Create an order")
        self.id = json.loads(response.text)['id']
        self.orderId = json.loads(response.text)['orderId']
        self.actual_state = "created"

    def assemble_order(self):
        data = json.dumps({'orderId': self.id})
        self.client.post('/order/assemble', headers=self.headers,
                         data=data, name="Assemble an order")
        self.actual_state = "assembled"

    def cancel_order(self):
        data = json.dumps({'orderId': self.id})
        self.client.post('/order/cancel', headers=self.headers,
                         data=data, name="Cancel an order")
        self.final_state = True
        self.actual_state = "cancelled"

    def deliver_order(self):
        data = json.dumps({'orderId': self.id})
        self.client.post('/order/deliver', headers=self.headers,
                         data=data, name="Assemble an order")
        self.actual_state = "delivered"

    def invalidate_order(self):
        data = json.dumps({'orderId': self.id})
        self.client.post('/order/invalidate', headers=self.headers,
                         data=data, name="Invalidate an order")
        self.final_state = True
        self.actual_state = "invalid"

    def release_invoice(self):
        data = json.dumps({'orderId': self.id})
        self.client.post('/order/releaseInvoice', headers=self.headers,
                         data=data, name="Release an order invoice")
        self.actual_state = "invoiced"

    def payment_on_delivery(self):
        data = json.dumps({'orderId': self.id})
        self.client.post('/order/paymentOnDelivery', headers=self.headers,
                         data=data, name="Pays during a delivery")
        self.final_state = True
        self.actual_state = "paid"

    def claim_order(self):
        data = json.dumps({'orderId': self.id})
        self.client.post('/order/claim', headers=self.headers,
                         data=data, name="Claims an order")
        self.actual_state = "returned"

    def receive_payment(self):
        data = json.dumps({'orderId': self.id})
        self.client.post('/order/receivePayment', headers=self.headers,
                         data=data, name="Pays an order")
        self.final_state = True
        self.actual_state = "paid"

    def reassemble_order(self):
        data = json.dumps({'orderId': self.id})
        self.client.post('/order/reassemble', headers=self.headers,
                         data=data, name="Reassemble an order")
        self.actual_state = "assembled"

    @seq_task(1)
    def step1(self):
        self.init_data()
#        print("Ordem {} ----- STEP 1 ----- CREATE ORDER".format(self.id))
        self.create_order()

    @seq_task(2)
    def step2(self):
        if not self.final_state:
            decision = True if randint(0, 99) < 96 else False
            if decision:
#                print("Ordem {} ----- STEP 2 ----- ASSEMBLE ORDER ----- ACTUAL STATE: '{}'".format(self.id, self.actual_state))
                self.assemble_order()
            else:
#                print("Ordem {} ----- STEP 2 ----- ASSEMBLE ORDER ----- ACTUAL STATE: '{}'".format(self.id, self.actual_state))
                self.cancel_order()

    @seq_task(3)
    def step3(self):
        if not self.final_state:
            decision = True if randint(0, 96) < 93 else False
            if decision:
#                print("Ordem {} ----- STEP 3 ----- DELIVER ORDER ----- ACTUAL STATE: '{}'".format(self.id, self.actual_state))
                self.deliver_order()
            else:
#                print("Ordem {} ----- STEP 3 ----- INVALIDATE ORDER ----- ACTUAL STATE: '{}'".format(self.id, self.actual_state))
                self.invalidate_order()

    @seq_task(4)
    def step4(self):
        if not self.final_state:
            decision = randint(0, 93)
            if decision < 76:
#                print("Ordem {} ----- STEP 4 ----- RELEASE INVOICE ----- ACTUAL STATE: '{}'".format(self.id, self.actual_state))
                self.release_invoice()
            elif 75 < decision < 90:
#                print("Ordem {} ----- STEP 4 ----- PAGAR ORDER ----- ACTUAL STATE: '{}'".format(self.id, self.actual_state))
                self.payment_on_delivery()
            else:
#                print("Ordem {} ----- STEP 4 ----- CLAIM ORDER ----- ACTUAL STATE: '{}'".format(self.id, self.actual_state))
                self.claim_order()

    @seq_task(5)
    def step5(self):
        if not self.final_state:
            if self.actual_state == "invoiced":
#                print("Ordem {} ----- STEP 5 ----- RECEIVE PAYMENT----- ACTUAL STATE: '{}'".format(self.id, self.actual_state))
                self.receive_payment()
            elif self.actual_state == "returned":
                decision = randint(0, 2)
                if decision < 2:
#                    print("Ordem {} ----- STEP 5 ----- REASSEMBLE ORDER ----- ACTUAL STATE: '{}'".format(self.id, self.actual_state))
                    self.reassemble_order()
                else:
#                    print("Ordem {} ----- STEP 5 ----- CANCEL ORDER ----- ACTUAL STATE: '{}'".format(self.id, self.actual_state))
                    self.cancel_order()

    @seq_task(6)
    def step6(self):
        if not self.final_state:
#            print("Ordem {} ----- STEP 6 ----- AUTO DELIVER ORDER ----- ACTUAL STATE: '{}'".format(self.id, self.actual_state))
            self.deliver_order()
#            print("Ordem {} ----- STEP 6 ----- AUTO INVOICE ORDER ----- ACTUAL STATE: '{}'".format(self.id, self.actual_state))
            self.release_invoice()
#            print("Ordem {} ----- STEP 6 ----- AUTO RECEIVE PAYMENT ORDER ----- ACTUAL STATE: '{}'".format(self.id, self.actual_state))
            self.receive_payment()


class StoreUser(HttpLocust):
    task_set = UserBehaviour
    min_wait = 5000
    max_wait = 15000
